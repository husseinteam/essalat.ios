// To parse the JSON, add this file to your project and do:
//
//   let cities = try Cities(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.citiesTask(with: url) { cities, response, error in
//     if let cities = cities {
//       ...
//     }
//   }
//   task.resume()

import Foundation

class Cities: Codable {
    let cities: [City]?
    
    init(cities: [City]?) {
        self.cities = cities
    }
}

class City: Codable {
    let id, name, stateID: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case stateID = "state_id"
    }
    
    init(id: String?, name: String?, stateID: String?) {
        self.id = id
        self.name = name
        self.stateID = stateID
    }
}

// MARK: Convenience initializers and mutators

extension Cities {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(Cities.self, from: data)
        self.init(cities: me.cities)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        cities: [City]?? = nil
        ) -> Cities {
        return Cities(
            cities: cities ?? self.cities
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension City {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(City.self, from: data)
        self.init(id: me.id, name: me.name, stateID: me.stateID)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        id: String?? = nil,
        name: String?? = nil,
        stateID: String?? = nil
        ) -> City {
        return City(
            id: id ?? self.id,
            name: name ?? self.name,
            stateID: stateID ?? self.stateID
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
