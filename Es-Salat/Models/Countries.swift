// To parse the JSON, add this file to your project and do:
//
//   let countries = try Countries(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.countriesTask(with: url) { countries, response, error in
//     if let countries = countries {
//       ...
//     }
//   }
//   task.resume()

import Foundation

class Countries: Codable {
    let countries: [Country]?
    
    init(countries: [Country]?) {
        self.countries = countries
    }
}

class Country: Codable {
    let id: Int?
    let sortname, name: String?
    let phoneCode: Int?
    
    init(id: Int?, sortname: String?, name: String?, phoneCode: Int?) {
        self.id = id
        self.sortname = sortname
        self.name = name
        self.phoneCode = phoneCode
    }
}

// MARK: Convenience initializers and mutators

extension Countries {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(Countries.self, from: data)
        self.init(countries: me.countries)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        countries: [Country]?? = nil
        ) -> Countries {
        return Countries(
            countries: countries ?? self.countries
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Country {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(Country.self, from: data)
        self.init(id: me.id, sortname: me.sortname, name: me.name, phoneCode: me.phoneCode)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        id: Int?? = nil,
        sortname: String?? = nil,
        name: String?? = nil,
        phoneCode: Int?? = nil
        ) -> Country {
        return Country(
            id: id ?? self.id,
            sortname: sortname ?? self.sortname,
            name: name ?? self.name,
            phoneCode: phoneCode ?? self.phoneCode
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
