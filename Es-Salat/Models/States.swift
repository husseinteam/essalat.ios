// To parse the JSON, add this file to your project and do:
//
//   let states = try States(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.statesTask(with: url) { states, response, error in
//     if let states = states {
//       ...
//     }
//   }
//   task.resume()

import Foundation

class States: Codable {
    let states: [State]?
    
    init(states: [State]?) {
        self.states = states
    }
}

class State: Codable {
    let id, name, countryID: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case countryID = "country_id"
    }
    
    init(id: String?, name: String?, countryID: String?) {
        self.id = id
        self.name = name
        self.countryID = countryID
    }
}

// MARK: Convenience initializers and mutators

extension States {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(States.self, from: data)
        self.init(states: me.states)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        states: [State]?? = nil
        ) -> States {
        return States(
            states: states ?? self.states
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension State {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(State.self, from: data)
        self.init(id: me.id, name: me.name, countryID: me.countryID)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        id: String?? = nil,
        name: String?? = nil,
        countryID: String?? = nil
        ) -> State {
        return State(
            id: id ?? self.id,
            name: name ?? self.name,
            countryID: countryID ?? self.countryID
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
