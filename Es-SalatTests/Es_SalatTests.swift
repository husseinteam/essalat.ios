//
//  Es_SalatTests.swift
//  Es-SalatTests
//
//  Created by Hüseyin Sönmez on 6.01.2019.
//  Copyright © 2019 Hüseyin Sönmez. All rights reserved.
//

import XCTest
@testable import Es_Salat

class Es_SalatTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCountries() {
        let expectation = self.expectation(description: "Country")
        if let countries_url = URL(string: "https://github.com/hiiamrohit/Countries-States-Cities-database/blob/master/countries.json") {
            URLSession.shared.countriesTask(with: countries_url) { countries, response, error in
                if let countries = countries {
                    XCTAssert(countries.countries?.count ?? 0 > 0)
                }
                XCTAssertNil(error)
                expectation.fulfill()
            }.resume()
        }
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testCities() {
        let expectation = self.expectation(description: "City")
        if let cities_url = URL(string: "https://github.com/hiiamrohit/Countries-States-Cities-database/blob/master/cities.json") {
            URLSession.shared.citiesTask(with: cities_url) { cities, response, error in
                if let cities = cities {
                    XCTAssert(cities.cities?.count ?? 0 > 0)
                }
                XCTAssertNil(error)
                expectation.fulfill()
                }.resume()
        }
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testStates() {
        let expectation = self.expectation(description: "State")
        if let states_url = URL(string: "https://github.com/hiiamrohit/Countries-States-Cities-database/blob/master/states.json") {
            URLSession.shared.statesTask(with: states_url) { states, response, error in
                if let states = states {
                    XCTAssert(states.states?.count ?? 0 > 0)
                }
                XCTAssertNil(error)
                expectation.fulfill()
                }.resume()
        }
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
